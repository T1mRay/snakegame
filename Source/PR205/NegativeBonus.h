// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Iteractable.h"
#include "NegativeBonus.generated.h"

UCLASS()
class PR205_API ANegativeBonus : public AActor, public IIteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANegativeBonus();

	UPROPERTY(EditDefaultsOnly)
		float TurnDownSpeed = 20.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIshead) override;
};
