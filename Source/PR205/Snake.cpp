// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Iteractable.h"
// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMovedirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	IsMoved = false;
	if (IsMoved == false)
	{
		IsMoved = true;
		Move();
	}
	if (IsAddElementNextTick==true)
	{
		AddSnakeElementNextTick();
	}
	
	IsAddElementNextTick = false;
	/*SetActorTickInterval(MovementSpeed);*/
}

void ASnake::AddSnakeElement(int ElementNum)
{
	for (int i = 0; i < ElementNum; ++i)
	{
		FVector NewLoaction(FVector(SnakeElements.Num() * ElementSize, 0, 0));
		FTransform NewTransform = FTransform(NewLoaction);
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		SnakeElement->SnakeOwner = this;
		int32 ElemIndex =  SnakeElements.Add(SnakeElement);
		if (ElemIndex == 0)
		{
			SnakeElement->SetFirstElementType();
		}
	}
	LocationOfLastElement = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
}

void ASnake::Move()
{
		
		FVector MovementVector(FVector::ZeroVector);

		switch (LastMovedirection)
		{
		case EMovementDirection::UP:
		{
			MovementVector.X += ElementSize;
			break;
		}
		case EMovementDirection::DOWN:
		{
			MovementVector.X -= ElementSize;
			break;
		}
		case EMovementDirection::LEFT:
		{
			MovementVector.Y += ElementSize;
			break;
		}
		case EMovementDirection::RIGHT:
		{
			MovementVector.Y -= ElementSize;
			break;
		}
		default:
			break;
		}
		//AddActorWorldOffset(MovementVector);

		LocationOfLastElement = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		SnakeElements[0]->ToggleCollision();
		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			auto CurrentElement = SnakeElements[i];
			auto PrevElement = SnakeElements[i - 1];
			FVector PrevLocation = PrevElement->GetActorLocation();
			CurrentElement->SetActorLocation(PrevLocation);
		}

		SnakeElements[0]->AddActorWorldOffset(MovementVector); ///
		SnakeElements[0]->ToggleCollision();
	
}

void ASnake::TickToCreateNewElementInSnake()
{
	IsAddElementNextTick = true;
}

void ASnake::AddSnakeElementNextTick()
{
	FTransform NewTransform = FTransform(LocationOfLastElement);
	ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	SnakeElement->SnakeOwner = this;
	int32 ElemIndex = SnakeElements.Add(SnakeElement);
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement,ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IIteractable* InteractableInterface = Cast<IIteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}



