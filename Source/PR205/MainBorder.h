// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Iteractable.h"
#include "MainBorder.generated.h"

UCLASS()
class PR205_API AMainBorder : public AActor, public IIteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMainBorder();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIshead) override;
};
