// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PR205GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PR205_API APR205GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
