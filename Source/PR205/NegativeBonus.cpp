// Fill out your copyright notice in the Description page of Project Settings.


#include "NegativeBonus.h"
#include "Snake.h"

// Sets default values
ANegativeBonus::ANegativeBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANegativeBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANegativeBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANegativeBonus::Interact(AActor* Interactor, bool bIshead)
{
	if (bIshead)
	{
		auto Snake = Cast<ASnake>(Interactor);;
		if (IsValid(Snake))
		{
			Snake->MovementSpeed -= TurnDownSpeed;
			this->Destroy();
		}
	}
}